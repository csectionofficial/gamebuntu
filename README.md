# Gamebuntu

Transform an Ubuntu install into a complete game-ready (!) setup.

## Installation

```sh
bash <(curl -fsL https://github.com/AFK-OS/una/raw/main/install.sh); una install gamebuntu-bin
```

## Updating

```sh
una update; una upgrade
```