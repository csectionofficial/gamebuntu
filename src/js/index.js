document.getElementById('apps-view').addEventListener('dom-ready', () => {
    document.getElementById('apps-view').insertCSS('body { padding-right: 15px; !important }')
})

_open = (category) => {
    document.getElementById('apps-view').setAttribute('src', category + ".html")
    Array.from(document.querySelectorAll('[class^=active],[class*=" active"]')).forEach((el) => el.classList.remove(`active-${el.id}`))
    document.getElementById(category).classList.add(`active-${category}`)
}

_open('launchers')