const { app, BrowserWindow } = require('electron')
const path = require('path')

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    width: 850,
    height: 600,
    title: 'Gamebuntu',
    minWidth: 700,
    minHeight: 500,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      webviewTag: true
    }
  })

  mainWindow.loadFile('html/index.html')

  // mainWindow.setMenu(null)
}

app.whenReady().then(() => {
  createWindow()
})