#!/bin/bash

if [ "$(dpkg-query -W --showformat='${db:Status-Status}' noisetorch-bin 2>&1)" = installed ]; then
  echo 'NoiseTorch is already installed.'
  sleep 3 && exit
fi

sudo apt-get install -y software-properties-common
sudo add-apt-repository -y universe
sudo add-apt-repository -y multiverse
una install noisetorch-bin